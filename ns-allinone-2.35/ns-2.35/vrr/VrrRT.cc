#include "ip.h"
#include "VrrCommon.h"
#include "VrrRT.h"


VRRRT::VRRRT() {

}

void
VRRRT::print(Trace* out) {

}

void
VRRRT::print() {
	D(printf("eA:\teB:\tnA:\tnB:\tid:\n");
    for (rtable_t::iterator it = rt_.begin(); it != rt_.end(); it++) {
        printf("%d\t%d\t%d\t%d\t%d\n", (*it).eA, (*it).eB, (*it).nA, (*it).nB, (*it).id);
    })
}

void
VRRRT::clear() {
    rt_.clear();
}

//void
//VRRRT::rm_entry(nsaddr_t dest) {
//	rt_.erase(dest);
//}
//
//void
//VRRRT::add_entry(nsaddr_t dest, nsaddr_t next) {
//	rt_[dest] = next;
//}

route*
VRRRT::rm_entry(nsaddr_t eA, int id) {
	route* r = getRoute(eA, id);
	rt_.erase(std::remove_if(rt_.begin(), rt_.end(), compareRoute(eA, id)), rt_.end());
	return r;
}

bool
VRRRT::add_entry(route r) {
    // check for duplicate
	if(!hasRoute(r.eA, r.id)){
	    rt_.push_back(r);
	    return true;
	}
	return false;
}

bool
VRRRT::add_nbEntry(route r) {
    // check for duplicate
    for (rtable_t::iterator it = rt_.begin(); it != rt_.end(); it++) {
    	if(((*it).eA==r.eA)&&((*it).id == r.id)&&((*it).eB == r.eB)){
    		return false;
    	}
    }
    rt_.push_back(r);
    return true;
}

route*
VRRRT::rm_nbEntry(nsaddr_t ea, nsaddr_t eb, int pid) {
	route* r = getRoute(ea, eb, pid);
	rt_.erase(std::remove_if(rt_.begin(), rt_.end(), compareNbRoute(ea, eb, pid)), rt_.end());
	return r;
}

bool VRRRT::hasRoute(nsaddr_t ea, int pid){
    for (rtable_t::iterator it = rt_.begin(); it != rt_.end(); it++) {
    	if(((*it).eA==ea)&&((*it).id == pid)){
    		return true;
    	}
    }
    return false;
}

route* VRRRT::getRoute(nsaddr_t ea, int pid){
    for (rtable_t::iterator it = rt_.begin(); it != rt_.end(); it++) {
    	if(((*it).eA==ea)&&((*it).id == pid)){
    		return &(*it);
    	}
    }
    return NULL;
}

route* VRRRT::getRoute(nsaddr_t ea, nsaddr_t eb, int pid){
    for (rtable_t::iterator it = rt_.begin(); it != rt_.end(); it++) {
    	if(((*it).eA==ea)&&((*it).id == pid)&&((*it).eB==eb)){
    		return &(*it);
    	}
    }
    return NULL;
}

std::vector<route> VRRRT::getRoutes(nsaddr_t toId){
	std::vector<route> ret;
	route r;
	for (rtable_t::iterator it = rt_.begin(); it != rt_.end(); it++){
        r = *it;
		if((r.eA == toId) || (r.eB == toId)){
			ret.push_back(r);
		}
	}
	return ret;
}

nsaddr_t
VRRRT::nextHop(nsaddr_t dest) {
//	route r = minRoute(dest);
//    nsaddr_t nxt;
//
//    int a_dist = abs(r.eA-dest);
//    int b_dist = abs(r.eB-dest);
//    if(a_dist < b_dist)
//        nxt= r.nA;
//    else
//        nxt= r.nB;
//
	int tmpdist = std::numeric_limits<int>::max(), dist = std::numeric_limits<int>::max();
	int eA, eB, nA, nB, nh = RT_NULL;

    for (rtable_t::iterator it = rt_.begin(); it != rt_.end(); it++) {
        eA = (*it).eA;
        eB = (*it).eB;
        nA = (*it).nA;
        nB = (*it).nB;

        if(abs(eA-dest) <= abs(eB-dest)){
        	tmpdist = abs(eA-dest);

            if(tmpdist <= dist){
            	dist = tmpdist;
            	nh = nA;
            }
        }
        else{
        	tmpdist = abs(eB-dest);

            if(tmpdist <= dist){
            	dist = tmpdist;
            	nh = nB;
            }
        }
    }
	D(printf("lookup of %d: nextHop=%d\n",dest,nh);)
    return nh;
}

nsaddr_t VRRRT::nextHopExclude(nsaddr_t dest, nsaddr_t exclude){
	int tmpdist = std::numeric_limits<int>::max(), dist = std::numeric_limits<int>::max();
	int eA, eB, nA, nB, nh = RT_NULL;

    for (rtable_t::iterator it = rt_.begin(); it != rt_.end(); it++) {
        eA = (*it).eA;
        eB = (*it).eB;
        nA = (*it).nA;
        nB = (*it).nB;

        if((eA == exclude) || (eB == exclude)){
        	continue;
        }

        if(abs(eA-dest) <= abs(eB-dest)){
        	tmpdist = abs(eA-dest);

            if(tmpdist <= dist){
            	dist = tmpdist;
            	nh = nA;
            }
        }
        else{
        	tmpdist = abs(eB-dest);

            if(tmpdist <= dist){
            	dist = tmpdist;
            	nh = nB;
            }
        }
    }
	D(printf("lookup of %d excluding %d: nextHopExclude=%d\n",dest,exclude,nh);)
    return nh;
}

u_int32_t
VRRRT::size() {
    return rt_.size();
}
