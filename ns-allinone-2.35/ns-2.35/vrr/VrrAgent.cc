/*
 * VRR.cpp
 *
 *  Created on: Dec 5, 2013
 *      Author: pgu
 */

#include "VrrPkt.h"
#include "VrrAgent.h"

/*
 * packet tcl class
 */
int hdr_vrr::offset_;
static class VRRHeaderClass: public PacketHeaderClass {
public:
        VRRHeaderClass() : PacketHeaderClass("PacketHeader/VRR",
                                              sizeof(hdr_all_vrr)) {
	  bind_offset(&hdr_vrr::offset_);
	}


} class_rtProtoVRR_hdr;

/*
 * timer for hello msgs
 */
void
VrrHelloTimer::handle(Event*) {
   agent_->sendHello();
   Scheduler::instance().schedule(this, &intr_, HELLOINTERVALL+JITTER);
}

/*
 * time for purging
 */
void
VrrPurgeTimer::handle(Event*) {
	agent_->markFailed();
	agent_->purgeInactive();

   Scheduler::instance().schedule(this, &intr_, PURGEINTERVALL+JITTER);
}

/*
 * time for partitioning
 */
void
VrrPartTimer::handle(Event*) {
	agent_->makeActive();
   Scheduler::instance().schedule(this, &intr_, PARTTIMEOUT+JITTER);
}

void VRR::makeActive(){
	if(!initialized && (index==1)){
		D(printf("VRR AGENT(%d) creating new partition\n", index);)
		initialized = true;
	}
}

void VRR::markFailed(){
	for(std::map<nsaddr_t, NodeState >::const_iterator it = nbStates.begin(); it != nbStates.end(); ++it){
		if(!isActive(it->first, PURGEINTERVALL)){
			nbStates[it->first] = FAILED;
			D(printf("VRR AGENT(%d) marked %d failed\n", index, it->first);)
		}
	}
}

void VRR::purgeInactive(){
	for(std::map<nsaddr_t, NodeState >::const_iterator it = nbStates.begin(); it != nbStates.end(); ++it){
		if(!isActive(it->first, 2*PURGEINTERVALL)){
			nbStates.erase(it->first);
			D(printf("VRR AGENT(%d) purged %d due to inactivity\n", index, it->first);)
		}
	}
}

VRR::VRR(nsaddr_t id) :
		Agent(PT_VRR), htimer(this), ptimer(this), patimer(this) {
	index = id;
	node = (MobileNode*) Node::get_node_by_address(id);
	ifqueue = 0;
	logtarget = 0;
	nextUID = 0;
	initialized = false;


	std::srand(CURRENT_TIME); //use current time as seed for random generator
	D(printf("VRR AGENT(%d)\n", index);)
}

/*
 * main rcv ->delegate to rcv* handlers
 */
void VRR::recv(Packet *p, Handler *h) {

	struct hdr_cmn *ch = HDR_CMN(p);
	struct hdr_ip *ih = HDR_IP(p);

	if(	ch->ptype() == PT_VRR) {
		D(printf("VRR AGENT(%d) received a vrr pkt\n", index);)
			ih->ttl_ -= 1;
			recvVrr(p);
			return;
	}

	D(printf("VRR AGENT(%d) received a pkt\n", index);)

	if((ih->saddr() == index) && (ch->num_forwards() == 0)) {
	 /*
	  * Add the IP Header.
	  * TCP adds the IP header too, so to avoid setting it twice, we check if
	  * this packet is not a TCP or ACK segment.
	  */
	  if (ch->ptype() != PT_TCP && ch->ptype() != PT_ACK) {
	    ch->size() += IP_HDR_LEN;
	  }
	   // Added by Parag Dadhania && John Novatnack to handle broadcasting
	  if ( (u_int32_t)ih->daddr() != IP_BROADCAST) {
	    ih->ttl_ = IP_DEF_TTL;
	  }
	}
	 /*
	  *  I received a packet that I sent.  Probably
	  *  a routing loop.
	  */
	else if(ih->saddr() == index) {
	   D(printf("VRR AGENT(%d) dropping pkt because of routing loop\n", index);)
	   drop(p, DROP_RTR_ROUTE_LOOP);
	   return;
	 }
	 /*
	  *  Packet I'm forwarding...
	  */
	 else {
	 /*
	  *  Check the TTL.  If it is zero, then discard.
	  */
	   if(--ih->ttl_ == 0) {
		 D(printf("VRR AGENT(%d) dropping pkt because ttl=0\n", index);)
	     drop(p, DROP_RTR_TTL);
	     return;
	   }
	 }

	forward(p);
}

/*
 * handle vrr packet
 */
void VRR::recvVrr(Packet *p) {
	struct hdr_ip* ih = HDR_IP(p);
	struct hdr_vrr* vh = HDR_VRR(p);

	switch (vh->pkt_type) {
		case VRR_HELLO:
			recvHello(p);
			break;
		case VRR_SETUP:
			recvSetup(p);
			break;
		case VRR_SETUPREQ:
			recvSetupreq(p);
			break;
		case VRR_SETUPFAIL:
			recvSetupfail(p);
			break;
		case VRR_TEARDOWN:
			recvTeardown(p);
			break;
	}
}

/*
 * forward packet
 */
void VRR::forward(Packet *p) {

    struct hdr_cmn* ch = HDR_CMN(p);
    struct hdr_ip* ih = HDR_IP(p);

    if (ch->direction() == hdr_cmn::UP &&
            ((u_int32_t) ih->daddr() == IP_BROADCAST || ih->daddr() == index)) {
        dmux->recv(p, 0);
        return;
    }
    else {
        ch->direction() = hdr_cmn::DOWN;
        ch->addr_type() = NS_AF_INET;

        if ((u_int32_t) ih->daddr() == IP_BROADCAST)
            ch->next_hop() = IP_BROADCAST;
        else {
            nsaddr_t next_hop = rtable.nextHop(ih->daddr());
            if (next_hop == IP_BROADCAST) {
                D(printf("%f: Agent %d can not forward a packet destined to %d\n", CURRENT_TIME, index, ih->daddr());)
                drop(p, DROP_RTR_NO_ROUTE);
                return;
            } else {
                ch->next_hop() = next_hop;
            }
        }

        Scheduler::instance().schedule(target_, p, 0.0);
		D(printf("VRR AGENT(%d) forwarding pkt\n", index);)
    }
}

/*
 * tcl command parsing
 */
int VRR::command(int argc, const char* const * argv) {
	D(printf("VRR AGENT(%d) received cmd %s\n", index, argv[1]);)
	  if(argc == 2) {
	  Tcl& tcl = Tcl::instance();

	  // start cmd
	  if(strncasecmp(argv[1], "start", 2) == 0) {
	      htimer.handle((Event*) 0);
	      ptimer.handle((Event*) 0);
	      patimer.handle((Event*) 0);
	      return TCL_OK;
	     }
	  }
	  if(argc == 3) {
	  // set if-queue
		    if(strcmp(argv[1], "if-queue") == 0) {
		    ifqueue = (PriQueue*) TclObject::lookup(argv[2]);

		      if(ifqueue == 0)
			return TCL_ERROR;
		      return TCL_OK;
		    }
		    else if (strcmp(argv[1], "port-dmux") == 0) {
		        	dmux = (PortClassifier *)TclObject::lookup(argv[2]);
		    	if (dmux == 0) {
		    		D(fprintf(stderr, "%s: %s lookup of %s failed\n", __FILE__, argv[1], argv[2]);)
		    		return TCL_ERROR;
		    	}
		    	return TCL_OK;
		    }
		    else if(strcmp(argv[1], "log-target") == 0 || strcmp(argv[1], "tracetarget") == 0) {
		          logtarget = (Trace*) TclObject::lookup(argv[2]);
		          if(logtarget == 0)
		    	return TCL_ERROR;
		          return TCL_OK;
		    }
	  }
	  return Agent::command(argc, argv);
}

/*
 * send hello msg
 */
void VRR::sendHello() {

    Packet* p = allocpkt();
    struct hdr_cmn* ch = HDR_CMN(p);
    struct hdr_ip* ih = HDR_IP(p);
    struct hdr_vrr_hello* vhh = HDR_VRR_HELLO(p);

	ch->ptype() = PT_VRR;
	ch->direction() = hdr_cmn::DOWN;
	ch->size() = IP_HDR_LEN + vhh->size();
	ch->error() = 0;
	ch->next_hop() = IP_BROADCAST;
	ch->addr_type() = NS_AF_NONE;
	ch->iface() = -2;

	ch->prev_hop_ = index;          // AODV hack

	ih->saddr() = index;
	ih->daddr() = IP_BROADCAST;
	ih->sport() = RT_PORT;
	ih->dport() = RT_PORT;
	ih->ttl() = 1;

    vhh->pkt_type = VRR_HELLO;
    vhh->active_ = initialized;

    std::set<nsaddr_t> linkedAndActive, linked, pending;
    linkedAndActive = getActiveNbs(LINKED);
    linked = getInactiveNbs(LINKED);
    pending = getNbs(PENDING);

    int elements = 0;
    std::fill((vhh->linkedAndActiveNodeIds()), (vhh->linkedAndActiveNodeIds())+SETSIZE, RT_NULL);
    for (std::set<nsaddr_t>::iterator it=linkedAndActive.begin(); it!=linkedAndActive.end(); it++){
    	if(elements < SETSIZE){
    		(vhh->linkedAndActiveNodeIds())[elements] = *it;
    		elements++;
    	}
    }
    elements = 0;
    std::fill((vhh->linkedNodeIds()), (vhh->linkedNodeIds())+SETSIZE, RT_NULL);
    for (std::set<nsaddr_t>::iterator it=linked.begin(); it!=linked.end(); it++){
    	if(elements < SETSIZE){
    		(vhh->linkedNodeIds())[elements] = *it;
    		elements++;
    	}
    }
    elements = 0;
    std::fill((vhh->pendingNodeIds()), (vhh->pendingNodeIds())+SETSIZE, RT_NULL);
    for (std::set<nsaddr_t>::iterator it=pending.begin(); it!=pending.end(); it++){
    	if(elements < SETSIZE){
    		(vhh->pendingNodeIds())[elements] = *it;
    		elements++;
    	}
    }

    Scheduler::instance().schedule(target_, p, 0.0);

	D(printf("VRR AGENT(%d) sending HELLO ", index);
	DP(std::cout<<"(LA: ";for(int i=0;i<SETSIZE;i++){
		std::cout<<(vhh->linkedAndActiveNodeIds())[i]<<" ";
	}
	std::cout<<"; L: ";
	for(int i=0;i<SETSIZE;i++){
		std::cout<<(vhh->linkedNodeIds())[i]<<" ";
	}
	std::cout<<"; P: ";
	for(int i=0;i<SETSIZE;i++){
		std::cout<<(vhh->pendingNodeIds())[i]<<" ";
	}
	std::cout<<") ";)
    std::cout << "ch PT=" << ch->ptype() << " dir=" << ch->direction() << " size=" << ch->size() << " nextHop=" << ch->next_hop() << std::endl;)

}

void VRR::sendSetupreq(nsaddr_t src, nsaddr_t dst, nsaddr_t proxy, std::set<nsaddr_t> vsetCopy) {

    // fill pkt
    Packet* p = allocpkt();
    struct hdr_cmn* ch = HDR_CMN(p);
    struct hdr_ip* ih = HDR_IP(p);
    struct hdr_vrr_setupreq* vhsr = HDR_VRR_SETUPREQ(p);

	ih->saddr() = index;
	ih->daddr() = IP_BROADCAST;
	ih->sport() = RT_PORT;
	ih->dport() = RT_PORT;
	ih->ttl() = 1;

    ch->ptype() = PT_VRR;
    ch->direction() = hdr_cmn::DOWN;
    ch->size() = IP_HDR_LEN + vhsr->size();
    ch->error() = 0;
	ch->next_hop() = IP_BROADCAST;
	ch->iface() = -2;
    ch->addr_type() = NS_AF_NONE;

	ch->prev_hop_ = index;          // AODV hack


    vhsr->pkt_type = VRR_SETUPREQ;
    vhsr->dst = dst;
    vhsr->src = src;
    vhsr->proxy = proxy;

	std::fill((vhsr->vset_), (vhsr->vset_)+SETSIZE, RT_NULL);
    if(!vsetCopy.empty()){
		int elements = 0;
		for (std::set<nsaddr_t>::iterator it=vsetCopy.begin(); it!=vsetCopy.end(); it++){
			if(elements < SETSIZE){
				(vhsr->vset_)[elements] = *it;
				elements++;
			}
		}
    }

    double j = JITTER;
    Scheduler::instance().schedule(target_, p, j);

	D(printf("VRR AGENT(%d) sending SETUPREQ (ih-src: %d, dst: %d, src: %d, proxy: %d, jitter: %f)\n", index, ih->saddr(), vhsr->dst, vhsr->src, vhsr->proxy, j);)
}

void VRR::sendSetup(nsaddr_t src, nsaddr_t dst, int pid, nsaddr_t proxy, std::set<nsaddr_t> vsetCopy) {
    Packet* p = allocpkt();
    struct hdr_cmn* ch = HDR_CMN(p);
    struct hdr_ip* ih = HDR_IP(p);
    struct hdr_vrr_setup* vhs = HDR_VRR_SETUP(p);

	ih->saddr() = index;
	ih->daddr() = dst;
	ih->sport() = RT_PORT;
	ih->dport() = RT_PORT;
	ih->ttl() = IP_DEF_TTL;

    ch->ptype() = PT_VRR;
    ch->direction() = hdr_cmn::DOWN;
    ch->size() = IP_HDR_LEN + vhs->size();
    ch->error() = 0;
	ch->next_hop() = IP_BROADCAST;
	ch->iface() = -2;
    ch->addr_type() = NS_AF_NONE;

	ch->prev_hop_ = index;          // AODV hack

    vhs->pkt_type = VRR_SETUP;
    vhs->dst = dst;
    vhs->src = src;
    vhs->proxy = proxy;

	std::fill((vhs->vset_), (vhs->vset_)+SETSIZE, RT_NULL);
    if(!vsetCopy.empty()){
		int elements = 0;
		for (std::set<nsaddr_t>::iterator it=vsetCopy.begin(); it!=vsetCopy.end(); it++){
			if(elements < SETSIZE){
				(vhs->vset_)[elements] = *it;
				elements++;
			}
		}
    }

    Scheduler::instance().schedule(target_, p, 0.0);

	D(printf("VRR AGENT(%d) sending SETUP (ih-src: %d, dst: %d, src: %d, proxy: %d)\n", index, ih->saddr(), vhs->dst, vhs->src, vhs->proxy);)

}

void VRR::sendSetupfail(nsaddr_t src, nsaddr_t dst, nsaddr_t proxy, std::set<nsaddr_t> vsetCopy) {

    Packet* p = allocpkt();
    struct hdr_cmn* ch = HDR_CMN(p);
    struct hdr_ip* ih = HDR_IP(p);
    struct hdr_vrr_setupfail* vhsf = HDR_VRR_SETUPFAIL(p);

	ch->ptype() = PT_VRR;
	ch->direction() = hdr_cmn::DOWN;
	ch->size() = IP_HDR_LEN + vhsf->size();
	ch->error() = 0;
	ch->next_hop() = dst;
	ch->addr_type() = NS_AF_INET;

	ih->saddr() = index;
	ih->daddr() = dst;
	ih->sport() = RT_PORT;
	ih->dport() = RT_PORT;
	ih->ttl() = IP_DEF_TTL;

    vhsf->pkt_type = VRR_SETUPFAIL;
    vhsf->dst = dst;
    vhsf->src = src;
    vhsf->proxy = proxy;

	std::fill((vhsf->vset_), (vhsf->vset_)+SETSIZE, RT_NULL);
    if(!vsetCopy.empty()){
		int elements = 0;
		for (std::set<nsaddr_t>::iterator it=vsetCopy.begin(); it!=vsetCopy.end(); it++){
			if(elements < SETSIZE){
				(vhsf->vset_)[elements] = *it;
				elements++;
			}
		}
    }

    Scheduler::instance().schedule(target_, p, 0.0);

	D(printf("VRR AGENT(%d) sending SETUPFAIL (ih-src: %d, dst: %d, src: %d, proxy: %d)\n", index, ih->saddr(), vhsf->dst, vhsf->src, vhsf->proxy);)

}

void VRR::sendTeardown(nsaddr_t src, nsaddr_t dst, int pid, nsaddr_t ea, std::set<nsaddr_t> vsetCopy){
    Packet* p = allocpkt();
    struct hdr_cmn* ch = HDR_CMN(p);
    struct hdr_ip* ih = HDR_IP(p);
    struct hdr_vrr_teardown* vhtd = HDR_VRR_TEARDOWN(p);

	ch->ptype() = PT_VRR;
	ch->direction() = hdr_cmn::DOWN;
	ch->size() = IP_HDR_LEN + vhtd->size();
	ch->error() = 0;
	ch->next_hop() = dst;
	ch->addr_type() = NS_AF_INET;

	ih->saddr() = index;
	ih->daddr() = dst;
	ih->sport() = RT_PORT;
	ih->dport() = RT_PORT;
	ih->ttl() = IP_DEF_TTL;

    vhtd->pkt_type = VRR_TEARDOWN;
    vhtd->dst = dst;
    vhtd->src = src;
    vhtd->pid = pid;
    vhtd->ea = ea;

	std::fill((vhtd->vset_), (vhtd->vset_)+SETSIZE, RT_NULL);
    if(!vsetCopy.empty()){
		int elements = 0;
		for (std::set<nsaddr_t>::iterator it=vsetCopy.begin(); it!=vsetCopy.end(); it++){
			if(elements < SETSIZE){
				(vhtd->vset_)[elements] = *it;
				elements++;
			}
		}
    }

    Scheduler::instance().schedule(target_, p, 0.0);

	D(printf("VRR AGENT(%d) sending TEARDOWN (ih-src: %d, dst: %d, src: %d, pid: %d, ea: %d)\n", index, ih->saddr(), vhtd->dst, vhtd->src, vhtd->pid, vhtd->ea);)

}

void VRR::teardownPathTo(nsaddr_t toId){
	std::vector<route> routes = rtable.getRoutes(toId);
	for (rtable_t::iterator it = routes.begin(); it != routes.end(); it++){
		teardownPath((*it).eA, (*it).id, RT_NULL);
	}
}

void VRR::teardownPath(nsaddr_t ea, nsaddr_t pid, nsaddr_t sender){
	route* rem = rtable.rm_entry(ea, pid);
	std::set<nsaddr_t> vsetCopy;
	if(rem->nA && inPset(rem->nA)){
		vsetCopy = sender!=RT_NULL ? vset : vsetCopy;
		sendTeardown(index, rem->nA, rem->id, rem->eA, vsetCopy);
	}
	if(rem->nB && inPset(rem->nB)){
		vsetCopy = sender!=RT_NULL ? vset : vsetCopy;
		sendTeardown(index, rem->nB, rem->id, rem->eA, vsetCopy);
	}
	if((sender!=RT_NULL) && inPset(sender)){
		vsetCopy = sender!=RT_NULL ? vset : vsetCopy;
		sendTeardown(index, sender, rem->id, rem->eA, vsetCopy);
	}
}

/*
 * hello packet handler
 */
void VRR::recvHello(Packet *p) {

	    struct hdr_ip* ih = HDR_IP(p);
	    struct hdr_vrr_hello* vhh = HDR_VRR_HELLO(p);

		D(printf("VRR AGENT(%d) received HELLO from %d", index, ih->saddr());
			DP(printf(" (Active: %d; LA: ", vhh->active_);
	    	for(int i=0;i<SETSIZE;i++){
	    		std::cout<<vhh->linkedAndActiveNodeIds()[i]<<" ";
	    	}
	    	std::cout<<"; L: ";
	    	for(int i=0;i<SETSIZE;i++){
	    		std::cout<<vhh->linkedNodeIds()[i]<<" ";
	    	}
	    	std::cout<<"; P: ";
	    	for(int i=0;i<SETSIZE;i++){
	    		std::cout<<vhh->pendingNodeIds()[i]<<" ";
	    	}
	    	std::cout<<")";)
	    	std::cout<<"\n";)

	    // All routing messages are sent from and to port RT_PORT,
	    // so we check it.
	    assert(ih->sport() == RT_PORT);
	    assert(ih->dport() == RT_PORT);

	    // update lastHello
	    lastHello[ih->saddr()] = CURRENT_TIME;

	    // update lastHello
	    active[ih->saddr()] = vhh->active_;

	    // update nb states
	    updateNbState(ih->saddr(), vhh->linkedAndActiveNodeIds(), vhh->linkedNodeIds(), vhh->pendingNodeIds());

	    // trigger setup if not init
	    if(!initialized && vhh->active_ && (nbStates[ih->saddr()]==LINKED)){
	    	nsaddr_t proxy = ih->saddr();
	    	sendSetupreq(index, index, proxy, vset);
	    }

	    // Release resources
	    Packet::free(p);
}

/*
 * setup packet handler
 */
void VRR::recvSetup(Packet *p) {

	    struct hdr_ip* ih = HDR_IP(p);
	    struct hdr_vrr_setup* vhs = HDR_VRR_SETUP(p);
		nsaddr_t* vsp = vhs->vset();
		std::set<nsaddr_t> vsetc(&vsp[0], &vsp[(SETSIZE-1)]);

	    nsaddr_t nh = (inPset(vhs->dst)) ? vhs->dst : rtable.nextHop(vhs->proxy);
	    route r = {vhs->src, vhs->dst, ih->saddr(), nh, vhs->pid};
	    bool added = rtable.add_entry(r);

		D(printf("VRR AGENT(%d) received SETUP (from: %d, to: %d, pid: %d, proxy: %d, inPset(dst): %d, nh: %d)\n", index, vhs->src, vhs->dst, vhs->pid, vhs->proxy, (inPset(vhs->dst)), nh);)

	    if(!added || !inPset(ih->saddr())){
	    	teardownPath(vhs->pid, vhs->src, ih->saddr());
	    }
	    else if(vhs->dst==index){
	    	added = addToVset(vhs->src, vsetc);
	    	if(!added){
		    	teardownPath(vhs->pid, vhs->src, RT_NULL);
	    	}
	    }
	    else if(nh != RT_NULL){
	    	sendSetup(vhs->src, vhs->dst, vhs->pid, vhs->proxy, vsetc);
	    }
	    else{
	    	if(!added){
		    	teardownPath(vhs->pid, vhs->src, RT_NULL);
	    	}
	    }

	    // Release resources
	    Packet::free(p);
}

/*
 * setupreq packet handler
 */
void VRR::recvSetupreq(Packet *p) {

	    struct hdr_ip* ih = HDR_IP(p);
	    struct hdr_vrr_setupreq* vhsr = HDR_VRR_SETUPREQ(p);

	    nsaddr_t nh = rtable.nextHopExclude(vhsr->dst, vhsr->src);

	    if(nh==ih->saddr()) nh = RT_NULL;

		D(printf("VRR AGENT(%d) received SETUPREQ (ih-src: %d, dst: %d, src: %d, proxy: %d, nh: %d)\n", index, ih->saddr(), vhsr->dst, vhsr->src, vhsr->proxy, nh);)

		nsaddr_t* vsp = vhsr->vset();
		std::set<nsaddr_t> vsetc(&vsp[0], &vsp[(SETSIZE-1)]);

		if((nh!=RT_NULL)||(vhsr->src==index)){
			sendSetupreq(vhsr->src, vhsr->dst, vhsr->proxy, vsetc);
		}
		else{
			std::set<nsaddr_t> ovset = vset;
			bool added = addToVset(vhsr->src, vsetc);

			if(added){
				sendSetup(index, vhsr->src, newPid(), vhsr->proxy, ovset);
			}
			else{
				sendSetupfail(index, vhsr->src, vhsr->proxy, ovset);
			}
		}

	    // Release resources
	    Packet::free(p);
}

/*
 * setupfail packet handler
 */
void VRR::recvSetupfail(Packet *p) {

	    struct hdr_ip* ih = HDR_IP(p);
	    struct hdr_vrr_setupfail* vhsf = HDR_VRR_SETUPFAIL(p);

		D(printf("VRR AGENT(%d) received SETUPFAIL from %d\n", index, ih->saddr());)

	    // Release resources
	    Packet::free(p);
}

nsaddr_t VRR::pickRndActive(){
	std::set<nsaddr_t> nodes = getActiveNbs(LINKED);
	std::set<int>::iterator i = nodes.begin();
	if(nodes.size() == 1){
		return *i;
	}
	else{
		int rndval = std::rand() % (nodes.size()-1);
		std::advance(i, rndval);
		return *i;
	}
}

bool VRR::shouldAdd(nsaddr_t id){
	if(id == index){
		return false;
	}
	if((id < index-VSET_CARD/2) || (id > index + VSET_CARD/2)){
		return false;
	}
	for (std::set<nsaddr_t>::iterator it=vset.begin(); it!=vset.end(); it++){
		if(id == *it){
			return false;
		}
	}
	return true;
}

bool VRR::addToVset(nsaddr_t src, std::set<nsaddr_t> vsetCopy){

	for (std::set<nsaddr_t>::iterator it=vsetCopy.begin(); it!=vsetCopy.end(); it++){
		if(shouldAdd(*it) && (*it != RT_NULL)){
			D(printf("VRR AGENT(%d) determined it should add %d\n", index, *it);)
			nsaddr_t proxy = pickRndActive();
			if(proxy != RT_NULL) sendSetupreq(index, *it, proxy, vset);
		}
	}

	if((src != RT_NULL) && shouldAdd(src)){

		// add src to vset and any nodes removed to rem
		vset.insert(src);
		initialized = (initialized)?initialized:true;
		D(printf("VRR AGENT(%d) added %d to VSET\n", index, src);)
		std::vector<nsaddr_t> rem;
		if(vset.size()>VSET_CARD){
			if(src < index){
				rem.push_back(*vset.begin());
				vset.erase(*vset.begin());
			}
			if(src > index){
				rem.push_back(*vset.end());
				vset.erase(*vset.end());
			}
		}

		// for each (id ∈ rem) TearDownPathTo(id)
		for (std::vector<nsaddr_t>::iterator it=rem.begin(); it!=rem.end(); it++){
			teardownPathTo(*it);
		}

		printVset();

		return true;
	}

	printVset();

	return false;

//	std::pair<std::set<nsaddr_t>::iterator,bool> ret = vset.insert(id);
//	return ret.second;
}

void VRR::printVset(){
	D(printf("VRR AGENT(%d) VSET: ", index);
	for (std::set<nsaddr_t>::iterator it=vset.begin(); it!=vset.end(); it++){
			std::cout<<*it<<" ";
	}
	std::cout<<"\n";)
}

int VRR::newPid(){
	return nextUID++;
}

/*
 * teardown packet handler
 */
void VRR::recvTeardown(Packet *p) {

	    struct hdr_ip* ih = HDR_IP(p);
	    struct hdr_vrr_teardown* vht = HDR_VRR_TEARDOWN(p);

		D(printf("VRR AGENT(%d) received TEARDOWN from %d\n", index, ih->saddr());)

	    // Release resources
	    Packet::free(p);
}

NodeState VRR::getStateFromHello(NodeList linkedAndActive, NodeList linked, NodeList pending){
	for(int i=0;i<SETSIZE;i++){
		if(linkedAndActive[i]==index){
			return LINKED;
		}
		if(linked[i]==index){
			return LINKED;
		}
		if(pending[i]==index){
			return PENDING;
		}
	}
	return MISSING;
}

void VRR::updateNbState(nsaddr_t id, NodeList linkedAndActive, NodeList linked, NodeList pending){

	NodeState msgState = getStateFromHello(linkedAndActive, linked, pending);

	D(printf("VRR AGENT(%d) updating nbStates (rcv: %d, own: %d)\n", index, msgState, nbStates[id]);)

	if(nbStates.count(id) == 0){
		// UNKNOWN state
		nbStates[id] = UNKNOWN;
	}

	if(nbStates[id]==UNKNOWN){
		switch(msgState){
			case LINKED:
				nbStates[id] = FAILED;
				break;
			case PENDING:
				nbLinkedAndActive(id);
				break;
			case MISSING:
				nbStates[id] = PENDING;
				break;
			default:
				D(printf("VRR AGENT(%d) internal state error\n", index);)
				break;
		}
	}
	else if (nbStates[id] == PENDING){
		switch(msgState){
			case LINKED:
				nbLinkedAndActive(id);
				break;
			case PENDING:
				nbLinkedAndActive(id);
				break;
			case MISSING:
				nbStates[id] = PENDING;
				break;
			default:
				D(printf("VRR AGENT(%d) internal state error\n", index);)
				break;
		}
	}
	else if (nbStates[id] == LINKED){
		switch(msgState){
			case LINKED:
				nbLinkedAndActive(id);
				break;
			case PENDING:
				nbLinkedAndActive(id);
				break;
			case MISSING:
				nbStates[id] = FAILED;
				break;
			default:
				D(printf("VRR AGENT(%d) internal state error\n", index);)
				break;
		}
	}
	else if (nbStates[id] == FAILED){
		switch(msgState){
			case LINKED:
				nbStates[id] = FAILED;
				break;
			case PENDING:
				nbStates[id] = PENDING;
				break;
			case MISSING:
				nbStates[id] = PENDING;
				break;
			default:
				D(printf("VRR AGENT(%d) internal state error\n", index);)
				break;
		}
	}

	D(printf("VRR AGENT(%d) updated nbStates: ", index);
	for(std::map<nsaddr_t, NodeState >::const_iterator it = nbStates.begin(); it != nbStates.end(); ++it)
	{
	    std::cout << it->first <<"="<<it->second<<" ";
	}
	std::cout << "\n";)
}

bool VRR::isActive(nsaddr_t id, double time){
	if((CURRENT_TIME - lastHello[id]) < time){
			return true;
	}
	return false;
}

void VRR::nbLinkedAndActive(nsaddr_t nb){
	nbStates[nb] = LINKED;
	insertNbPaths(nb);
}

void VRR::nbNotLinked(nsaddr_t nb, NodeState newState){
	nbStates[nb] = newState;
	removeNbPaths(nb);
}

std::set<nsaddr_t> VRR::getActiveNbs(NodeState ns){
	std::set<nsaddr_t> ret;
	for(std::map<nsaddr_t, NodeState >::const_iterator it = nbStates.begin(); it != nbStates.end(); ++it){
		if(it->second == ns){
			if(active[it->first]){
				ret.insert(it->first);
			}
		}
	}
	return ret;
}

std::set<nsaddr_t> VRR::getInactiveNbs(NodeState ns){
	std::set<nsaddr_t> ret;
	for(std::map<nsaddr_t, NodeState >::const_iterator it = nbStates.begin(); it != nbStates.end(); ++it){
		if(it->second == ns){
			if(!active[it->first]){
				ret.insert(it->first);
			}
		}
	}
	return ret;
}

std::set<nsaddr_t> VRR::getNbs(NodeState ns){
	std::set<nsaddr_t> ret;
	for(std::map<nsaddr_t, NodeState >::const_iterator it = nbStates.begin(); it != nbStates.end(); ++it){
		if(it->second == ns){
			ret.insert(it->first);
		}
	}
	return ret;
}

/*void VRR::addToPset(nsaddr_t node) {

	D(printf("VRR AGENT(%d) adding %d to pset (current: ", index, node);
    for (std::set<int>::iterator it = pset.begin(); it != pset.end(); ++it) std::cout <<*it<<" ";
    std::cout <<")\n";)

    pset.insert(node);

    // add to rt
    route rt = {index, node, RT_NULL, node, NB_PATHID }; // ea, eb, na, nb, id
    rtable.add_entry(rt);
    D(rtable.print();)
}*/

void VRR::insertNbPaths(nsaddr_t id){
    route rt = {index, id, RT_NULL, id, NB_PATHID }; // ea, eb, na, nb, id
    rtable.add_nbEntry(rt);
    D(rtable.print();)
}

void VRR::removeNbPaths(nsaddr_t id){
    rtable.rm_nbEntry(index, id, NB_PATHID);
    D(rtable.print();)
}

bool VRR::inPset(nsaddr_t id){
	//linked and active means in pset
	//TODO maybe check for lastHello
	std::set<nsaddr_t> linkedNbs = getNbs(LINKED);

	D(printf("VRR AGENT(%d) PSET: ", index);
	for (std::set<nsaddr_t>::iterator it=linkedNbs.begin(); it!=linkedNbs.end(); it++){
			std::cout<<*it<<" ";
	}
	std::cout<<"\n";)

	if(linkedNbs.count(id) != 0){
		return true;
	}
	return false;
}

static class VrrClass: public TclClass {
public:
	VrrClass() :
			TclClass("Agent/VRR") {
	}
	TclObject* create(int argc, const char* const * argv) {
		assert(argc == 5);
		return (new VRR((nsaddr_t) Address::instance().str2addr(argv[4])));
	}
} class_rtProtoVRR;
