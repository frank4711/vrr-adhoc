/*
 * VrrPkt.h
 *
 *  Created on: Dec 5, 2013
 *      Author: pgu
 */

#ifndef VRRPKT_H_
#define VRRPKT_H_

#include <address.h>
#include <packet.h>
#include "VrrCommon.h"

/* =====================================================================
   Packet Formats...
   ===================================================================== */
#define VRR_HELLO  	0x01
#define VRR_SETUP  	0x02
#define VRR_SETUPREQ  	0x04
#define VRR_SETUPFAIL  	0x08
#define VRR_TEARDOWN  	0x10

/*
 * VRR Routing Protocol Header Macros
 */
#define HDR_VRR(p)		((struct hdr_vrr*)hdr_vrr::access(p))
#define HDR_VRR_HELLO(p)  	((struct hdr_vrr_hello*)hdr_vrr::access(p))
#define HDR_VRR_SETUP(p)  	((struct hdr_vrr_setup*)hdr_vrr::access(p))
#define HDR_VRR_SETUPREQ(p)  	((struct hdr_vrr_setupreq*)hdr_vrr::access(p))
#define HDR_VRR_SETUPFAIL(p)  	((struct hdr_vrr_setupfail*)hdr_vrr::access(p))
#define HDR_VRR_TEARDOWN(p)  	((struct hdr_vrr_teardown*)hdr_vrr::access(p))

/*
 * General VRR Header - shared by all formats
 */
struct hdr_vrr {
    u_int8_t        pkt_type;

	static int offset_; // required by PacketHeaderManager
	inline static int& offset() { return offset_; }
	inline static hdr_vrr* access(const Packet* p) {
		return (hdr_vrr*) p->access(offset_);
	}
};

/*
 * VRR Hello Header
 */
struct hdr_vrr_hello {
    u_int8_t        pkt_type;
	NodeList pendingNodeIds_;
	NodeList linkedAndActiveNodeIds_;
	NodeList linkedNodeIds_;
	bool active_;

	inline NodeList& pendingNodeIds() {return pendingNodeIds_;}
	inline NodeList& linkedAndActiveNodeIds() {return linkedAndActiveNodeIds_;}
	inline NodeList& linkedNodeIds() {return linkedNodeIds_;}

  inline int size() {
  int sz = 0;
  	sz = sizeof(hdr_vrr_hello);
  	assert (sz >= 0);
	return sz;
  }
};

/*
 * VRR setup Header
 */
struct hdr_vrr_setup {
    u_int8_t        pkt_type;
    nsaddr_t src;
    nsaddr_t dst;
    int pid;
    nsaddr_t proxy;
    NodeList vset_;

	inline NodeList& vset() {return vset_;}

  inline int size() {
  int sz = 0;
  	sz = sizeof(hdr_vrr_setup);
  	assert (sz >= 0);
	return sz;
  }
};

/*
 * VRR setupreq Header
 */
struct hdr_vrr_setupreq {
    u_int8_t        pkt_type;
    nsaddr_t src;
    nsaddr_t dst;
    nsaddr_t proxy;
    NodeList vset_;

	inline NodeList& vset() {return vset_;}

  inline int size() {
  int sz = 0;
  	sz = sizeof(hdr_vrr_setupreq);
  	assert (sz >= 0);
	return sz;
  }
};

/*
 * VRR setupfail Header
 */
struct hdr_vrr_setupfail {
    u_int8_t        pkt_type;
    nsaddr_t src;
    nsaddr_t dst;
    nsaddr_t proxy;
    NodeList vset_;

	inline NodeList& vset() {return vset_;}

  inline int size() {
  int sz = 0;
  	sz = sizeof(hdr_vrr_setupfail);
  	assert (sz >= 0);
	return sz;
  }
};

/*
 * VRR teardown Header
 */
struct hdr_vrr_teardown {
  u_int8_t        pkt_type;
  nsaddr_t src;
  nsaddr_t dst;
  nsaddr_t ea;
  int pid;
  NodeList vset_;

  inline NodeList& vset() {return vset_;}

  inline int size() {
  int sz = 0;
  	sz = sizeof(hdr_vrr_teardown);
  	assert (sz >= 0);
	return sz;
  }
};

/*
 * Union for hdr size calc
 */
union hdr_all_vrr {
  hdr_vrr          vh;
  hdr_vrr_hello  vhh;
  hdr_vrr_setup  vhs;
  hdr_vrr_setupreq  vhsr;
  hdr_vrr_setupfail  vhsf;
  hdr_vrr_teardown  vht;
};

#endif /* VRRPKT_H_ */
