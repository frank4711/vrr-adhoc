/*
 * VRR.h
 *
 *  Created on: Dec 5, 2013
 *      Author: pgu
 */

#ifndef VRR_H_
#define VRR_H_

#include <agent.h>
#include <packet.h>

#include <address.h>
#include <scheduler.h>
//#include <types.h>
#include <set>
#include <map>
#include <vector>
#include <algorithm>
#include <iostream>
#include <string>
#include <utility>
#include <iterator>
#include <ctime>
#include <cstdlib>
#include <random.h>
#include <mobilenode.h>
#include <classifier/classifier-port.h>
#include <tcl.h>
#include <priqueue.h>
#include <cmu-trace.h>
#include "VrrRT.h"

class VRR;

/*
 * Timer for broadcasting HELLOs
 */
class VrrHelloTimer: public Handler {
public:
	VrrHelloTimer(VRR* agent) : agent_(agent){}
    void	handle(Event*);
protected:
	VRR* agent_;
	Event	intr_;
};

/*
 * Timer for purging inactive nbs
 */
class VrrPurgeTimer: public Handler {
public:
	VrrPurgeTimer(VRR* agent) : agent_(agent){}
    void	handle(Event*);
protected:
	VRR* agent_;
	Event	intr_;
};

/*
 * Timer for creating new ring
 */
class VrrPartTimer: public Handler {
public:
	VrrPartTimer(VRR* agent) : agent_(agent){}
    void	handle(Event*);
protected:
	VRR* agent_;
	Event	intr_;
};

/*
 * VRR Agent
 */
class VRR: public Agent {

friend class VrrHelloTimer;
friend class VrrPurgeTimer;
friend class VrrPartTimer;
friend class VRRRT;

public:
	VRR(nsaddr_t id);
    void recv(Packet *p, Handler *);
    int command(int, const char *const *);
protected:
	void sendHello();
	void sendSetupreq(nsaddr_t src, nsaddr_t dst, nsaddr_t proxy, std::set<nsaddr_t> vsetCopy);
	void sendSetup(nsaddr_t src, nsaddr_t dst, int pid, nsaddr_t proxy, std::set<nsaddr_t> vsetCopy);
	void sendSetupfail(nsaddr_t src, nsaddr_t dst, nsaddr_t proxy, std::set<nsaddr_t> vsetCopy);
	void sendTeardown(nsaddr_t src, nsaddr_t dst, int pid, nsaddr_t ea, std::set<nsaddr_t> vsetCopy);
	void teardownPathTo(nsaddr_t toId);
	void teardownPath(nsaddr_t ea, nsaddr_t pid, nsaddr_t sender);
    void recvVrr(Packet *p);
    void recvHello(Packet *p);
    void recvSetup(Packet *p);
    void recvSetupreq(Packet *p);
    void recvSetupfail(Packet *p);
    void recvTeardown(Packet *p);
    void forward(Packet *p);

    bool isActive(nsaddr_t id, double time);
    std::set<nsaddr_t> getActiveNbs(NodeState);
    std::set<nsaddr_t> getInactiveNbs(NodeState);
    std::set<nsaddr_t> getNbs(NodeState);
    NodeState getStateFromHello(NodeList linkedAndActive, NodeList linked, NodeList pending);
    void updateNbState(nsaddr_t id, NodeList linkedAndActive, NodeList linked, NodeList pending);
    void insertNbPaths(nsaddr_t);
    void removeNbPaths(nsaddr_t);
    void nbLinkedAndActive(nsaddr_t);
    void nbNotLinked(nsaddr_t nb, NodeState newState);
    void markFailed();
    void purgeInactive();
    void makeActive();
    bool addToVset(nsaddr_t src, std::set<nsaddr_t> vsetCopy);
    bool shouldAdd(nsaddr_t id);
    nsaddr_t pickRndActive();
    int newPid();
    void printVset();

    bool inPset(nsaddr_t);

    std::set<nsaddr_t> vset;
    bool initialized;
    nsaddr_t index;
    MobileNode *node;

    // Hello timer
	VrrHelloTimer htimer;

	// purge timer
	VrrPurgeTimer ptimer;

    // Hello timer
	VrrPartTimer patimer;

	/* for passing packets up to agents */
	PortClassifier *dmux;

    /*
     * A pointer to the network interface queue that sits
     * between the "classifier" and the "link layer".
     */
    PriQueue        *ifqueue;

    /*
     * A mechanism for logging the contents of the routing
     * table.
     */
    Trace           *logtarget;

    // local rt
    VRRRT rtable;

    // internal nb state map
    std::map<nsaddr_t, NodeState> nbStates;

    // remember last hello
    std::map<nsaddr_t, double> lastHello;

    // remember activity state
    std::map<nsaddr_t, double> active;

    int nextUID;
};

#endif /* VRR_H_ */
