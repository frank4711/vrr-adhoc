/*
 * VrrCommon.h
 *
 *  Created on: Dec 5, 2013
 *      Author: pgu
 */

#ifndef VRRCOMMON_H_
#define VRRCOMMON_H_

#include <address.h>
#include <packet.h>
#include <limits>

#define DEBUG 1
#define SHOWPKG 1

#define SETSIZE 15
#define CURRENT_TIME Scheduler::instance().clock()
#define HELLOINTERVALL 1
#define PURGEINTERVALL (HELLOINTERVALL * 4 * 2)
#define PARTTIMEOUT (HELLOINTERVALL * 3)
#define CURRENT_TIME Scheduler::instance().clock()
#define JITTER ((std::rand()%10)*0.1)
#define NB_PATHID std::numeric_limits<int>::max()
#define VSET_CARD 4
#define RT_NULL (-1)
#define SEP "--------------------"

#ifdef DEBUG
#define D(x) x
#else
#define D(x)
#endif

#ifdef SHOWPKG
#define DP(x) x
#else
#define DP(x)
#endif

typedef nsaddr_t NodeList [SETSIZE];

enum NodeState{
	UNKNOWN, PENDING, LINKED, FAILED, MISSING
};

#endif /* VRRCOMMON_H_ */
