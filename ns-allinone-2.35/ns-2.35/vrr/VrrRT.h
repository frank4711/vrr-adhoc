#ifndef __VRRRT_h__
#define __VRRRT_h__

#include <vector>
#include <algorithm>
#include <trace.h>
//#include <functional>

struct route {
    nsaddr_t eA; // Endpoint A
    nsaddr_t eB; // Endpoint B
    nsaddr_t nA; // nH towards A
    nsaddr_t nB; // nH towards B
    int id; // pid
};

struct compareRoute {
	compareRoute(nsaddr_t id, int pid) : id(id), pid(pid) {}
	int operator()(route& r) { return ((id==r.eA) && (pid==r.id)); }

private:
	nsaddr_t id;
	int pid;
};

struct compareNbRoute {
	compareNbRoute(nsaddr_t ea, nsaddr_t eb, int pid) : ea(ea), eb(eb), pid(pid) {}
	int operator()(route& r) { return ((ea==r.eA) && (pid==r.id) && (eb == r.eB)); }

private:
	nsaddr_t ea, eb;
	int pid;
};

typedef std::vector<route> rtable_t;



class VRRRT {
   rtable_t rt_;

   public:
   VRRRT();
   void print(Trace*);
   void print();
   void clear();
   bool add_entry(route);
   bool add_nbEntry(route);
   route* rm_nbEntry(nsaddr_t ea, nsaddr_t eb, int pid);
   route* rm_entry(nsaddr_t,int);
   nsaddr_t lookup(nsaddr_t);

   nsaddr_t nextHopExclude(nsaddr_t dest, nsaddr_t exclude);
   nsaddr_t nextHop(nsaddr_t);
   u_int32_t size();
   route* getRoute(nsaddr_t ea, int pid);
   route* getRoute(nsaddr_t ea, nsaddr_t eb, int pid);
   bool hasRoute(nsaddr_t ea, int pid);
   std::vector<route> getRoutes(nsaddr_t toId);

};

#endif
