set dist(5m)  7.69113e-06
set dist(9m)  2.37381e-06
set dist(10m) 1.92278e-06
set dist(11m) 1.58908e-06
set dist(12m) 1.33527e-06
set dist(13m) 1.13774e-06
set dist(14m) 9.81011e-07
set dist(15m) 8.54570e-07
set dist(16m) 7.51087e-07
set dist(20m) 4.80696e-07
set dist(25m) 3.07645e-07
set dist(30m) 2.13643e-07
set dist(35m) 1.56962e-07
set dist(40m) 1.20174e-07
# ======================================================================
# Define options
# ======================================================================
set val(chan)         Channel/WirelessChannel  ;# channel type
set val(prop)         Propagation/TwoRayGround ;# radio-propagation model
set val(ant)          Antenna/OmniAntenna      ;# Antenna type
set val(ll)           LL                       ;# Link layer type
set val(ifq)          Queue/DropTail/PriQueue  ;# Interface queue type
set val(ifqlen)       50                       ;# max packet in ifq
set val(netif)        Phy/WirelessPhy          ;# network interface type
set val(mac)          Mac/802_11               ;# MAC type
set val(rp)           VRR                     ;# ad-hoc routing protocol 
set val(nn)           9                        ;# number of mobilenodes
set val(x)	      500
set val(y)	      500

Phy/WirelessPhy set CSThresh_ $dist(20m)
Phy/WirelessPhy set RXThresh_ $dist(20m)

set ns_ [new Simulator]
debug 1

set tracefd     [open vt3x3.tr w]
$ns_ trace-all $tracefd

set namtrace [open vt3x3.nam w]
$ns_ namtrace-all-wireless $namtrace $val(x) $val(y)

proc finish {} {
        global ns_ nf
        $ns_ flush-trace
        close $nf
        exec nam vt3x3.nam &
        exit 0
}

set topo [new Topography]
$topo load_flatgrid $val(x) $val(y)

# Create God
create-god $val(nn)

# Create channel #1
set chan_1 [new $val(chan)]

$ns_ node-config -adhocRouting $val(rp) \
		-llType $val(ll) \
		-macType $val(mac) \
		-ifqType $val(ifq) \
		-ifqLen $val(ifqlen) \
		-antType $val(ant) \
		-propType $val(prop) \
		-phyType $val(netif) \
		-topoInstance $topo \
		-agentTrace ON \
		-routerTrace ON \
		-macTrace ON \
		-movementTrace OFF \
		-channel $chan_1

#
# Provide initial (X,Y, for now Z=0) co-ordinates for mobilenodes
#

## init nodes
set c 0
for {set i 0} {$i < 3 } { incr i } {
for {set j 0} {$j < 3 } { incr j } {
	set node_($c) [$ns_ node ]
	$node_($c) random-motion 0

	set xx [expr 100+$i*15]
	set yy [expr 100+$j*15]

	$node_($c) set X_ $xx
	$node_($c) set Y_ $yy
	$node_($c) set Z_ 0.0

	# define the node size for nam
	$ns_ initial_node_pos $node_($c) 1

	set tt_($c) [expr $c]

	set rag_($c) [$node_($c) get-ragent]
        $ns_ at $tt_($c) "$rag_($c) start"

	set c [expr $c+1]
}
}

#
# Now produce some simple node movements
# Node_(1) starts to move towards node_(0)
#
# $ns_ at 3.0 "$node_(0) setdest 245.0 5.0 25.0"
# $ns_ at 3.0 "$node_(1) setdest 240.0 5.0 25.0"

# Node_(1) then starts to move away from node_(0)
# $ns_ at 20.0 "$node_(1) setdest 495.0 5.0 50.0"


# TCP connections between node_(0) and node_(3)

set tcp [new Agent/TCP]
$tcp set class_ 2
set sink [new Agent/TCPSink]
$ns_ attach-agent $node_(0) $tcp
$ns_ attach-agent $node_(4) $sink
$ns_ connect $tcp $sink
set ftp [new Application/FTP]
# $ftp produce 1
$ftp attach-agent $tcp
$ns_ at 15.0 "$ftp start" 


$ns_ at 30.0001 "stop"
$ns_ at 30.0002 "puts \"NS EXITING...\" ; $ns_ halt"
proc stop {} {
    global ns_ tracefd
    close $tracefd
}

puts "Starting Simulation..."
$ns_ run
